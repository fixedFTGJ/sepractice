#include<iostream>
#include<Windows.h>
#include<fstream>
#include<string>
#include<conio.h>
#include<time.h>

using namespace std;

const COORD TextFieldLeftUpperCorner = {0, 8};
const COORD TextFieldRightLowerCorner = {79, 19};
const COORD endDel = {79, 21};
const int TextFieldSize = 1040;
const COORD RulesFieldLefrUpperCorner = {0, 22};
const int wordsNumber = 13;
const int stringLength = 78;
const COORD AlphabetPos = {22, 6};
const COORD AttemptsPos = {78, 1};
const COORD AttemptsBoxPos = {59, 1};
const COORD ThemePos = {1, 1};
const COORD FirstLineEnd = {78, 1};
const COORD WordLineEnd = {78, 4};
const COORD LastLineEnd = {78, 6};
const COORD bottomStartPos = {0, 24};
const COORD bottomEndPos = {78, 24};
const COORD continueStartPos = {0, 23};
const COORD continueEndPos = {78, 23};

void ClearConsole(COORD startPos, COORD endPos);

void PrintTextFromFile(char *fileName, COORD position);

void DrawGameField();

void ExitApplication();

bool EnjoyTheGame(const char *themeFile);

void main()
{ 
	string themeFiles[9] = {"����\\������.txt", "����\\��������.txt", "����\\���������.txt", "����\\�����.txt",
		"����\\�����.txt", "����\\������.txt", "����\\��������.txt", "����\\�����.txt", "����\\��������������.txt"};

	int themeNumber = 0;

	SetConsoleTitle(L"Infernal Gallows");

	DrawGameField();

	srand(unsigned(time(NULL)));

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������� ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 9 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 9 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������� ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 8 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 8 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������� ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 7 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 7 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������ ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 6 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 6 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\����� ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 5 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 5 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\��������� ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 4 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 4 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������ ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 3 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 3 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������ ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 2 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 2 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	themeNumber = rand() % 9;

	PrintTextFromFile("�����\\������ ����.txt", TextFieldLeftUpperCorner);

	if(EnjoyTheGame(themeFiles[themeNumber].c_str()))
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		PrintTextFromFile("��������\\������������� �������� 1 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
	}
	else
	{
		PrintTextFromFile("��������\\������������� �������� 1 �����.txt", TextFieldLeftUpperCorner);
		system("pause");
		exit(0);
	}
	
	ClearConsole(TextFieldLeftUpperCorner, endDel);

	system("pause");
}

void ClearConsole(COORD startPos, COORD endPos)
{
	HANDLE h;
	h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(h, startPos);
	for(int x = startPos.X; x <= endPos.X; x++)
	{
		for(int y = startPos.Y; y <= endPos.Y; y++)
		{
			cout << " ";
		}
	}
	SetConsoleCursorPosition(h, startPos);
}

void PrintTextFromFile(char *fileName, COORD position)
{
	ifstream file;
	char buffer;
	string text;
	string temp;
	HANDLE h;
	int freeSpace = 0;
	unsigned int k = 0;
	int count = 0;
	unsigned int lastLetterPos = 0;

	file.open(fileName, ios::in);

	while(!file.eof())
	{
		file.get(buffer);
		text += buffer;
	}

	if (!text.empty())
	   text.erase( text.end() - 1);

	file.close();

	for(int i = position.Y; i <= TextFieldRightLowerCorner.Y; i++)
		for(int j = position.X; j <= TextFieldRightLowerCorner.X; j++)
			freeSpace++;

	h = GetStdHandle(STD_OUTPUT_HANDLE);

	if(unsigned(freeSpace) < text.length())
		for(unsigned int i = 0; i < text.length(); i++)
		{
			if(text[i] != ' ')
			{
				count++;
			}
			else
			{
				lastLetterPos = i-1;
				count++;
				if(count > freeSpace)
				{
					SetConsoleCursorPosition(h, position);
					cout << temp << endl;
					temp = "";
					count = 0;
					SetConsoleCursorPosition(h, continueStartPos);
					system("pause");
					ClearConsole(continueStartPos, continueEndPos);
					ClearConsole(TextFieldLeftUpperCorner, endDel);
					freeSpace = TextFieldSize;
					SetConsoleCursorPosition(h, TextFieldLeftUpperCorner);
				}
				for(k; k <= lastLetterPos; k++)
					temp += text[k];				
			}
			if(i == text.length() - 1)
			{
				for(k; k < text.length(); k++)
					temp += text[k];	
				SetConsoleCursorPosition(h, TextFieldLeftUpperCorner);
					cout << temp << endl;
			}
		}
	else
	{
		SetConsoleCursorPosition(h, position);
		cout << text << endl;
	}
	cout << "+ + +" << endl;
}

void DrawGameField()
{
	const int width = 79;
	const int heightOfTopField = 7;
	const int heightOfDownField = 22;

	HANDLE h;
	h = GetStdHandle(STD_OUTPUT_HANDLE);
	int coordOfx = 1;

	COORD positionOfLeftTopAngle = {0,0};
	COORD positionOfRightTopAngle = {width,coordOfx};
	COORD positionOfLeftDownAngle = {0,heightOfDownField};

	SetConsoleCursorPosition(h, positionOfLeftTopAngle);
	cout << char(218);
	for(int i = 1; i <= width -1; i++)
	{
		cout << char(196);
	}
	cout << char(191);
	SetConsoleCursorPosition(h, positionOfLeftTopAngle);
	cout << endl;
	for(int i = 1; i <= heightOfTopField -1; i++)
	{
		cout << char(179) << endl;
	}
	cout << char(192);
	for(int i = 1; i <= width -1; i++)
	{
		cout << char(196);
	}
	cout << char(217);
	for(positionOfRightTopAngle.Y; positionOfRightTopAngle.Y <= heightOfTopField -1; positionOfRightTopAngle.Y++)
	{
		SetConsoleCursorPosition(h, positionOfRightTopAngle);
		cout << char(179);	
	}
	cout <<endl;
	SetConsoleCursorPosition(h, positionOfLeftDownAngle);
	for(int i = 0; i <= width; i++)
	{
		cout << char(196);
	}
}

void ExitApplication()
{
	if(getch() == 27)
	{
		exit(0);
	}
}

bool EnjoyTheGame(const char *themeFile)
{
	ifstream theme;
	ifstream alphabet;
	ifstream themeBox;
	ifstream attemptsBox;
	int attempts = 5;
	int i = 0;
	int shift = 0;
	string word;
	char buffer;
	string temp = "";
	string words[wordsNumber];
	COORD lastCursorPos;
	COORD wordStartPos = {0, 4};
	COORD letterPos = {0, 4};
	COORD alphabetLetterPos = AlphabetPos;
	HANDLE h;
	char inputChar;
	string pool;
	int mistakeCount = 0;
	int winCount = 0;
	bool win = false;
	bool escape = false;
	int charCode = 0;

	CONSOLE_SCREEN_BUFFER_INFO bi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &bi);

	lastCursorPos.X = bi.dwCursorPosition.X;
	lastCursorPos.Y = bi.dwCursorPosition.Y;

	theme.open(themeFile, ios::in);

	while(!theme.eof())
	{
		theme.get(buffer);
		if(buffer == ' ')
		{
			if(temp.length() > 0)
			{
				words[i] = temp;
				temp = "";
				i++;
			}
		}
		else
			temp += buffer;
	}

	srand(unsigned(time(NULL)));

	word = words[rand() % wordsNumber];

	wordStartPos.X = (unsigned(stringLength) - word.length())/2;

	h = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleCursorPosition(h, wordStartPos);

	for(unsigned int j = 0; j < word.length(); j++)
		cout << "*";

	attemptsBox.open("���� ��������� �������.txt", ios::in);

	while(!attemptsBox.eof())
	{
		attemptsBox.get(buffer);
		temp += buffer;
	}

	attemptsBox.close();

	SetConsoleCursorPosition(h, AttemptsBoxPos);

	cout << temp << attempts;

	temp = "";

	themeBox.open("���� �����.txt", ios::in);

	themeBox >> temp;

	themeBox.close();

	SetConsoleCursorPosition(h, ThemePos);

	char *themeName = new char[25];

	CharToOemA(themeFile, themeName);

	cout << temp;

	i = 5;

	while(themeName[i] != '.')
	{
		cout << themeName[i];
		i++;
	}

	delete(themeName);
	temp = "";

	SetConsoleCursorPosition(h, AlphabetPos);

	alphabet.open("���������\\�������.txt", ios::in);

	while(!alphabet.eof())
	{
		alphabet.get(buffer);
		temp += buffer;
	}

	alphabet.close();

	cout << temp;
	
	bool condition = true;

	SetConsoleCursorPosition(h, bottomStartPos);

	cout << "Press Tab for Help";

	while(!escape)
	{
		inputChar = getch();

		charCode = int(inputChar);

		if((charCode <= -81) && (charCode >= -96))
			charCode -= 32;
		if((charCode <= -17) && (charCode >= -32))
			charCode -= 80;

		for(unsigned int j = 0; j < pool.length(); j++)
			if(int(pool[j]) == charCode)
				condition = false;

		switch(charCode)
		{
			case 27: 
				exit(0);
				break;
			case 9:
				PrintTextFromFile("�������.txt", lastCursorPos);
				GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &bi);
				lastCursorPos.X = bi.dwCursorPosition.X;
				lastCursorPos.Y = bi.dwCursorPosition.Y;
				break;
			default:
			{
				if(((charCode <= -97) && (charCode >= -128)) && (condition) && (!win))
				{
					pool += char(charCode);
					alphabetLetterPos.X = AlphabetPos.X + charCode + 128;
					SetConsoleCursorPosition(h, alphabetLetterPos);
					cout << " ";
					for(unsigned int j = 0; j < word.length(); j++)
					{
						if((charCode == int(word[j])) && (!win))
						{
							winCount++;
							letterPos.X = wordStartPos.X + j;
							SetConsoleCursorPosition(h, letterPos);
							cout << word[j];
							if(winCount == word.length())
							win = true;
						}
					else
						mistakeCount++;
					}
				}
				else
				{
					if(!condition)
						PrintTextFromFile("���������\\������������� �����.txt", lastCursorPos);
					else
						PrintTextFromFile("���������\\������ �����.txt", lastCursorPos);

					GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &bi);

					lastCursorPos.X = bi.dwCursorPosition.X;
					lastCursorPos.Y = bi.dwCursorPosition.Y;
				}
			}
		}

		condition = true;

		if(mistakeCount == word.length())
		{
			attempts--;
			SetConsoleCursorPosition(h, AttemptsPos);
			cout << attempts;
		}

		mistakeCount = 0;

		if(win || (attempts == 0))
			escape = true;
	}

	if(win)
	{
		SetConsoleCursorPosition(h, lastCursorPos);
		ClearConsole(ThemePos, FirstLineEnd);
		ClearConsole(wordStartPos, WordLineEnd);
		ClearConsole(AlphabetPos, LastLineEnd);
		ClearConsole(bottomStartPos, bottomEndPos);
		return true;
	}
	else
	{
		ClearConsole(TextFieldLeftUpperCorner, endDel);
		ClearConsole(bottomStartPos, bottomEndPos);
		return false;
	}
}